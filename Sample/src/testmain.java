import java.util.Scanner;

import java.sql.*;

public class testmain {

	public static void dash() {
		System.out.println(
				"----------------------------------------------------------------------------------------------------------------");
	}

	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub
		Scanner take = new Scanner(System.in);
		
		// JDBC Connectivity
		Class.forName("com.mysql.jdbc.Driver");
		Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/EmployeeDB", "root", "enter");
		
		// Using Statement
		Statement stmt = con.createStatement();
		ResultSet rs = stmt.executeQuery("select * from employee_details");
		while (rs.next()) {
			System.out.println(
					rs.getString(1) + "  " + rs.getString(2) + "  " + rs.getString(3) + "   " + rs.getString(4)+"  "+rs.getString(5) + "  " + rs.getString(6) + "  " + rs.getString(7) + "   " + rs.getString(8));
		}

		dash();
		
		// Using Prepared Statement
		PreparedStatement ps = con.prepareStatement("select * from employee_details");
		ResultSet rs1 = ps.executeQuery();
		while (rs1.next()) {
			System.out.println(
					rs1.getString(1) + "  " + rs1.getString(2) + "  " + rs1.getString(3) + "   " + rs1.getString(4)+"  "+rs1.getString(5) + "  " + rs1.getString(6) + "  " + rs1.getString(7) + "   " + rs1.getString(8));
		}
		
		// Insertion using prepared Statement
		ps = con.prepareStatement("insert into employee_details values (?,?,?,?,?,?,?,?)");
		Employee e = new Employee("105", "Harry", 28, "07/11/2020", "GPC", 2000000, 3,"Jagath");
		ps.setString(1, e.getEmp_no());
		ps.setString(4,e.getEmp_name() );
		ps.setInt(2,e.getAge() );
		ps.setString(3,e.getDob() );
		ps.setString(6, e.getProject_name());
		ps.setDouble(7,e.getSalary() );
		ps.setInt(8, e.getYears_of_expereince());
		ps.setString(5, e.getEmployer_name());
		if(!ps.execute()) {
			System.out.println("Record Inserted");
		}
		con.commit();
	}

}

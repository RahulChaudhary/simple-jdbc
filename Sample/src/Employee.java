
public class Employee {
	
	private String emp_no;
	private String emp_name;
	private int age;
	private String dob;
	private String project_name;
	private float salary;
	private int years_of_expereince;
	private String employer_name;
	
	public Employee() {
		super();
	}

	public Employee(String emp_no, String emp_name, int age, String dob, String project_name, float salary,
			int years_of_expereince, String employer_name) {
		super();
		this.emp_no = emp_no;
		this.emp_name = emp_name;
		this.age = age;
		this.dob = dob;
		this.project_name = project_name;
		this.salary = salary;
		this.years_of_expereince = years_of_expereince;
		this.employer_name = employer_name;
	}

	public String getEmp_no() {
		return emp_no;
	}

	public void setEmp_no(String emp_no) {
		this.emp_no = emp_no;
	}

	public String getEmp_name() {
		return emp_name;
	}

	public void setEmp_name(String emp_name) {
		this.emp_name = emp_name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getProject_name() {
		return project_name;
	}

	public void setProject_name(String project_name) {
		this.project_name = project_name;
	}

	public float getSalary() {
		return salary;
	}

	public void setSalary(float salary) {
		this.salary = salary;
	}

	public int getYears_of_expereince() {
		return years_of_expereince;
	}

	public void setYears_of_expereince(int years_of_expereince) {
		this.years_of_expereince = years_of_expereince;
	}

	public String getEmployer_name() {
		return employer_name;
	}

	public void setEmployer_name(String employer_name) {
		this.employer_name = employer_name;
	}

	@Override
	public String toString() {
		return "Employee [emp_no=" + emp_no + ", emp_name=" + emp_name + ", age=" + age + ", dob=" + dob
				+ ", project_name=" + project_name + ", salary=" + salary + ", years_of_expereince="
				+ years_of_expereince + ", employer_name=" + employer_name + "]";
	}
	
	


}

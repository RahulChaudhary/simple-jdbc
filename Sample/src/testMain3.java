import java.sql.SQLException;

import javax.sql.rowset.JdbcRowSet;
import javax.sql.rowset.RowSetProvider;

public class testMain3 {

	public static void main(String[] args) throws SQLException, ClassNotFoundException {
		// TODO Auto-generated method stub
		
		Class.forName("com.mysql.jdbc.Driver");  
		
		//Using RowSet
		JdbcRowSet rowSet = RowSetProvider.newFactory().createJdbcRowSet();  
		rowSet.setUrl("jdbc:mysql://localhost:3306/EmployeeDB");  
		rowSet.setUsername("root");  
		rowSet.setPassword("enter");  
		           
		rowSet.setCommand("select * from employee_details");  
		rowSet.execute();  
		
		 while (rowSet.next()) {  
             // Generating cursor Moved event  
             System.out.println("Emp_no: " + rowSet.getString(1));  
             System.out.println("Age: " + rowSet.getString(2));  
             System.out.println("DOB: " + rowSet.getString(3));  
             System.out.println("Employee_name: " + rowSet.getString(4));
             System.out.println("Employer_name: " + rowSet.getString(5));  
             System.out.println("Project_name: " + rowSet.getString(6));  
             System.out.println("Salary: " + rowSet.getString(7));  
             System.out.println("Years_of_experience: " + rowSet.getString(8));
             System.out.println("-------------------------------------------------------------");
     }  
	}

}
